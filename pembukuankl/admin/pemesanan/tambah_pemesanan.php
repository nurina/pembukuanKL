<!doctype html>
<html lang="en">
<?php include "../../config/koneksi.php";
ini_set("display_error","0");
error_reporting(0);
session_start();

if (empty($_SESSION['username'])) {
	header('location:../../index.php');
}
?>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISTEM INFORMASI PEMBUKUAN KARYA LOGAM</title>
	<link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen" />
	<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/datepicker3.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen" />
	<link href="../js/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" media="screen" />
	<link href="../js/jquery-ui-1.11.4.custom/jquery-ui.min.css" rel="stylesheet" media="screen" />
	<link href="../js/jquery-ui-1.11.4.custom/jquery-ui.theme.css" rel="stylesheet" media="screen" />
	<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
<script src="../js/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script src="../js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>

<script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min1.js"></script>

 
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="../js/hideshow.js" type="text/javascript"></script>
	<script src="../js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/jquery.equalHeight.js"></script>

  

	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

<link rel="stylesheet" href="../js/jquery-ui.css">
  <script src="../js/jquery-1.10.2.js"></script>
  <script src="../js/jquery-ui.js"></script>
<script type="text/javascript">
			$(document).ready(function(){
				$("#nama_produk").autocomplete({
					minLength:2,
					source:'get_product.php',
					select:function(event, ui){
						$("#kode_produk").html(ui.item.nama);
					}
				});
			});
		</script>

<script type="text/javascript">
			$(document).ready(function(){
				$("#nama_produk1").autocomplete({
					minLength:2,
					source:'get_product.php',
					select:function(event, ui){
						$("#kode_produk").html(ui.item.nama);
					}
				});
			});
		</script>
<script type="text/javascript">
			$(document).ready(function(){
				$("#nama_produk2").autocomplete({
					minLength:2,
					source:'get_product.php',
					select:function(event, ui){
						$("#kode_produk").html(ui.item.nama);
					}
				});
			});
		</script>
<script type="text/javascript">
			$(document).ready(function(){
				$("#nama_produk3").autocomplete({
					minLength:2,
					source:'get_product.php',
					select:function(event, ui){
						$("#kode_produk").html(ui.item.nama);
					}
				});
			});
</script>

<script type="text/javascript">
			$(document).ready(function(){
				$("#nama_produk4").autocomplete({
					minLength:2,
					source:'get_product.php',
					select:function(event, ui){
						$("#kode_produk").html(ui.item.nama);
					}
				});
			});
		</script>

<script type="text/javascript">
			$(document).ready(function(){
				$("#nama_produk5").autocomplete({
					minLength:2,
					source:'get_product.php',
					select:function(event, ui){
						$("#kode_produk").html(ui.item.nama);
					}
				});
			});
		</script>

<script type="text/javascript">
			$(document).ready(function(){
				$("#nama_pelanggan").autocomplete({
					minLength:2,
					source:'get_pelanggan.php',
					select:function(event, ui){
						$("#kode_pelanggan").html(ui.item.nama);
					}
				});
			});
		</script>		

</head>


<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../index.php"><span>SISTEM INFORMASI PEMBUKUAN</span> KARYA LOGAM</a>
				
			</div>

		</div><!-- /.container-fluid -->
	</nav>
	
	<!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<h3>Pemasukan dan Pengeluaran Dana</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="../pembukuan/tambah_aktifitas.php">Tambah Kegiatan</a></li>
			<li class="icn_view_users"><a href="../pembukuan/lihat_kegiatan.php">Kelola Daftar Kegiatan</a></li>
			<li class="icn_folder"><a href="tambah_kategori.php">Tambah Kategori</a></li>
			<li class="icn_edit_article"><a href="lihat_kategori.php">Ubah Kategori</a></li>
			<li class="icn_categories"><a href="../pembukuan/rekap_harian.php">Rekap Harian</a></li>
			<li class="icn_categories"><a href="../pembukuan/laporan_bulanan_ku.php">Laporan Keuangan Bulanan</a></li>
			
		</ul>
		<h3>Daftar Pemesanan</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="tambah_pelanggan.php">Tambah Pelanggan</a></li>
			<li class="icn_edit_article"><a href="lihat_pelanggan.php">Ubah Pelanggan</a></li>
			<li class="icn_folder"><a href="tambah_produk.php">Tambah Produk</a></li>
			<li class="icn_edit_article"><a href="lihat_produk.php">Ubah Produk</a></li>
			<li class="icn_new_article"><a href="tambah_pemesanan.php">Tambah Pemesanan</a></li>
			<li class="icn_categories"><a href="lihat_pemesanan.php">Kelola Daftar Pemesanan</a></li>
			<li class="icn_categories"><a href="laporan_bulanan_pem.php">Laporan Pemesanan Bulanan</a></li>
			<li class="icn_tags"><a href="grafik2.php">Grafik Omset</a></li>
		</ul>
		<h3>Admin</h3>
		<ul class="toggle">
			
			<li class="icn_jump_back"><a href="../../config/logout.php">Keluar</a></li>
		</ul>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<section id="secondary_bar">
		
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="../index.php">KARYA LOGAM</a> <div class="breadcrumb_divider"></div> <a class="current">Tambah Pemesanan</a></article>
		</div>
	</section>
		
		
		
		
		
		<div class="clear"></div>
	<div class="row">
			<div class="col-md-6">
				
					
					<div class="panel-body">
					<form role="form" action="insert_pemesanan.php" method="POST">
					<div class="form-group">
								<?php 
	if(isset($_GET['isSuccess'])) { ?>
		<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Pemesanan Berhasil Ditambahkan</div>
	<?php }
	elseif(isset($_GET['isFailed'])) { ?>
	<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Terjadi Kesalahan Dalam Memasukkan Data</div>
	<?php }
	elseif(isset($_GET['isAngka'])) { ?>
	<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Jumlah Pesanan Tidak Valid</div>
	<?php }
	
	?>
								
								<div class="form-group">
									<label>Nama Pemesan</label>
									<span id="kode_pelanggan" style="visibility:hidden"></span>
									<input class="form-control" name="nama_pelanggan" id="nama_pelanggan" type="text" required oninvalid="this.setCustomValidity('kolom nama pemesan belum terisi')" />
								</div>
								<div class="form-group">
									<label for="nama_produk">Nama Produk</label><br>
									
									
  					<input class="form-control" name="nama_produk1" id="nama_produk1" placeholder="1" type="text" required oninvalid="this.setCustomValidity('kolom nama produk belum terisi')"/>
								<input class="form-control" name="jumlah_pesan1" type="number" placeholder="Jumlah Pesanan" required oninvalid="this.setCustomValidity('kolom jumlah pesanan belum terisi')" />
								

  					<input class="form-control" style="margin-top:10px" name="nama_produk2" id="nama_produk2" placeholder="2" type="text" />
								<input  class="form-control" name="jumlah_pesan2" type="number" placeholder="Jumlah Pesanan" />
								

  					<input class="form-control" style="margin-top:10px" name="nama_produk3" id="nama_produk3" placeholder="3" type="text" />
								<input  class="form-control" name="jumlah_pesan3" type="number" placeholder="Jumlah Pesanan" />
			
  					<input class="form-control" style="margin-top:10px" name="nama_produk4" id="nama_produk4" placeholder="4" type="text" />
								<input  class="form-control" name="jumlah_pesan4" type="number" placeholder="Jumlah Pesanan"  />

  					<input class="form-control" style="margin-top:10px" name="nama_produk5" id="nama_produk5" placeholder="5" type="text" />
								<input  class="form-control" name="jumlah_pesan5" type="number" placeholder="Jumlah Pesanan" />
								</div>
								
									
									
									
									
									
								
								
								<div class="form-group">
                <label>Tanggal</label>
                <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input class="form-control" size="10" type="date" name="tanggal" required oninvalid="this.setCustomValidity('kolom tanggal belum terisi')" />
     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
    <input type="hidden" id="dtp_input2" value=""/>
 
            </div>
								
								<div class="form-group">
									<label>Keterangan</label>
									<textarea class="form-control" rows="7" name="keterangan"></textarea>
								</div>
								
								<div class="form-group">
								</div>
								<button type="submit" class="btn btn-primary" name="simpan">Simpan</button>
								<button type="reset" class="btn btn-danger">Reset</button>
							</form>
								
					</div>
					</div>
				
			</div><!-- /.row -->

		</div><!--/.main-->
	</section>


</body>

		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/chart.min.js"></script>
		<script src="../js/chart-data.js"></script>
		<script src="../js/easypiechart.js"></script>
		<script src="../js/easypiechart-data.js"></script>
		<script src="../js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
		<script type="text/javascript" src="../js/locales/bootstrap-datetimepicker.id.js" charset="UTF-8"></script>
		
		<script type="text/javascript">

 $('.form_date').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script>


</html>