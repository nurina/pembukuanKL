<!doctype html>
<html lang="en">
<?php include "../../config/koneksi.php";
ini_set("display_error","0");
error_reporting(0);
session_start();

if (empty($_SESSION['username'])) {
	header('location:../../index.php');
}?>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISTEM INFORMASI PEMBUKUAN KARYA LOGAM</title>
	<link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen" />
	<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/datepicker3.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	

	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="../js/hideshow.js" type="text/javascript"></script>
	<script src="../js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

<script language="javascript">
var s5_taf_parent = window.location;
    function open_win_editar(targetBulan,targetTahun) {
        window.open ('preview_pem.php?bulan=' + targetBulan + '&tahun=' + targetTahun, 'page','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=750,height=600,left=50,top=50,titlebar=yes');
     }
</script>

</head>


<body>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../index.php"><span>SISTEM INFORMASI PEMBUKUAN</span> KARYA LOGAM</a>
			</div>

		</div><!-- /.container-fluid -->
	</nav>
	<!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<h3>Pemasukan dan Pengeluaran Dana</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="../pembukuan/tambah_aktifitas.php">Tambah Kegiatan</a></li>
			<li class="icn_view_users"><a href="../pembukuan/lihat_kegiatan.php">Kelola Daftar Kegiatan</a></li>
			<li class="icn_folder"><a href="tambah_kategori.php">Tambah Kategori</a></li>
			<li class="icn_edit_article"><a href="lihat_kategori.php">Ubah Kategori</a></li>
			<li class="icn_categories"><a href="../pembukuan/rekap_harian.php">Rekap Harian</a></li>
			<li class="icn_categories"><a href="../pembukuan/laporan_bulanan_ku.php">Laporan Keuangan Bulanan</a></li>
			
		</ul>
		<h3>Daftar Pemesanan</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="tambah_pelanggan.php">Tambah Pelanggan</a></li>
			<li class="icn_edit_article"><a href="lihat_pelanggan.php">Ubah Pelanggan</a></li>
			<li class="icn_folder"><a href="tambah_produk.php">Tambah Produk</a></li>
			<li class="icn_edit_article"><a href="lihat_produk.php">Ubah Produk</a></li>
			<li class="icn_new_article"><a href="tambah_pemesanan.php">Tambah Pemesanan</a></li>
			<li class="icn_categories"><a href="lihat_pemesanan.php">Kelola Daftar Pemesanan</a></li>
			<li class="icn_categories"><a href="laporan_bulanan_pem.php">Laporan Pemesanan Bulanan</a></li>
			<li class="icn_tags"><a href="grafik2.php">Grafik Omset</a></li>
			
		</ul>
		
		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_jump_back"><a href="../../config/logout.php">Keluar</a></li>
		</ul>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<section id="secondary_bar">
		
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="../index.php">KARYA LOGAM</a> <div class="breadcrumb_divider"></div> <a class="current">Laporan Pemesanan Bulanan</a></article>
		</div>
	</section>
		
		
		
		
		
		<div class="clear"></div>
		
		<div class="panel-body">
							<form method="post" action="" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xs-6 col-sm-3">
										<label>Bulan</label>
										<select class="form-control" name="bulan">
										<option value="" >Pilih Bulan</option>
										<option value="1" >Januari</option>	
										<option value="2" >Februari</option>	
										<option value="3" >Maret</option>	
										<option value="4" >April</option>	
										<option value="5" >Mei</option>	
										<option value="6" >Juni</option>	
										<option value="7" >Juli</option>	
										<option value="8" >Agustus</option>	
										<option value="8" >september</option>	
										<option value="10" >Oktober</option>	
										<option value="11" >November</option>	
										<option value="12" >Desember</option>	
										</select>
									</div>
									<div class="col-xs-6 col-sm-3">
										<label>Tahun</label>
										<select class="form-control" name="tahun">
										<option value="" >Pilih Tahun</option>
										<?php
											$tahun=2015;
											for ($i=$tahun; $i<=$tahun +50 ; $i++)
											echo "<option value=$i>$i<br>";
											echo "</option></select>";

												?>

									</div>
									<div class="col-xs-6 col-sm-3">
										<button style="margin-top:25px" class="btn btn-primary" type="submit" name="list_tampil">Submit</button>
									</div>
									
								</div>
							</form>
							<br>

<div class="dataTable_wrapper">
<?php
									if(isset($_POST['list_tampil'])){
										$bulan = $_POST['bulan'];
										$tahun = $_POST['tahun'];
										$query_tampil = mysql_query("SELECT tanggal,nama_produk,nama_pelanggan,jumlah_pesan,transaksi_pemesanan.harga, transaksi_pemesanan.harga*jumlah_pesan as total_harga,keterangan from transaksi_pemesanan, produk,pelanggan where MONTH(tanggal) = '$bulan' AND YEAR(tanggal)='$tahun' AND transaksi_pemesanan.kode_produk = produk.kode_produk AND transaksi_pemesanan.kode_pelanggan = pelanggan.kode_pelanggan ORDER BY date(tanggal), transaksi_pemesanan.kode_pelanggan");
										$query_omset = mysql_query("SELECT  sum(transaksi_pemesanan.harga*jumlah_pesan) as totalOmset from transaksi_pemesanan, produk,pelanggan where MONTH(tanggal) = '$bulan' AND YEAR(tanggal)='$tahun' AND transaksi_pemesanan.kode_produk = produk.kode_produk AND transaksi_pemesanan.kode_pelanggan = pelanggan.kode_pelanggan ORDER BY date(tanggal), transaksi_pemesanan.kode_pelanggan");						
$select_omset = mysql_fetch_array($query_omset);
$totalOmset = $select_omset['totalOmset'];
										if($bulan == '1'){$namabulan = "Januari";}
elseif ($bulan == '2') {$namabulan = "Februari";}
elseif ($bulan == '3') {$namabulan = "Maret";}
elseif ($bulan == '4') {$namabulan = "April";}
elseif ($bulan == '5') {$namabulan = "Mei";}
elseif ($bulan == '6') {$namabulan = "Juni";}
elseif ($bulan == '7') {$namabulan = "Juli";}
elseif ($bulan == '8') {$namabulan = "Agustus";}
elseif ($bulan == '9') {$namabulan = "September";}
elseif ($bulan == '10') {$namabulan = "Oktober";}
elseif ($bulan == '11') {$namabulan = "November";}
elseif ($bulan == '12') {$namabulan = "Desember";}
										?>
<form method="GET" action="" enctype="multipart/form-data">
										<div class="col-xs-6 col-sm-3" style="margin-buttom:25px">
<input style="margin-buttom:25px" type="button" value="Print dan Preview" onClick="open_win_editar(<?php echo $bulan; ?>,<?php echo $tahun; ?>)" />
</div>
</form>	
<br>
<div class="col-md-4" align="center">
<label style="">Rekap Laporan Pemesanan Bulanan</label>
<label style=""><?php echo "Bulan : ".$namabulan." | Tahun : ".$tahun; ?></label>
<br>
</div>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">

									
									
										<tr>
										<th class="center">Tanggal</th>
										<th class="center">Nama Pelanggan</th>
										<th class="center">Nama Produk</th>
										<th class="center">Jumlah Pesanan</th>
										<th class="center">Harga</th>
										<th class="center">Total Harga</th>
										<th class="center">Keterangan</th>


									</tr>
									<?php
									
										while($select_tampil = mysql_fetch_array($query_tampil)){
											$harga = $select_tampil['harga'];
											$total_harga = $select_tampil['total_harga'];
											$jumlah_desimal ="0";
											$pemisah_desimal =",";
											$pemisah_ribuan =".";
											
											?>
											<tr class="odd gradeX">
												<td><?php echo $select_tampil['tanggal']; ?></td>
												<td><?php echo $select_tampil['nama_pelanggan']; ?></td>
												<td><?php echo $select_tampil['nama_produk']; ?></td>
												<td><?php echo $select_tampil['jumlah_pesan']; ?></td>
												<td><?php echo "Rp ".number_format($harga, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></td>
												<td><?php echo "Rp ".number_format($total_harga, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></td>
												<td><?php echo $select_tampil['keterangan']; ?></td>
											</tr>
										<?php 
											} 
										
										?>			
									</table>
<ul style="list-style-type:none">
  
  <li><b>Total Omset			: <?php echo "Rp ".number_format($totalOmset, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></b></li>
</ul>
<?php
}
										?>
								</div>
		
		<div class="spacer"></div>
	</section>


</body>

</html>