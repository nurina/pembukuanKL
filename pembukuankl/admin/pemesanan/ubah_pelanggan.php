<!doctype html>
<html lang="en">
<?php include "../../config/koneksi.php";
ini_set("display_error","0");
error_reporting(0);
session_start();

if (empty($_SESSION['username'])) {
	header('location:../../index.php');
}
?>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISTEM INFORMASI PEMBUKUAN KARYA LOGAM</title>
	<link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen" />
	<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/datepicker3.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	

	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="../js/hideshow.js" type="text/javascript"></script>
	<script src="../js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../index.php"><span>SISTEM INFORMASI PEMBUKUAN</span> KARYA LOGAM</a>
				
			</div>

		</div><!-- /.container-fluid -->
	</nav>
	
	<!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<h3>Pemasukan dan Pengeluaran Dana</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="../pembukuan/tambah_aktifitas.php">Tambah Kegiatan</a></li>
			<li class="icn_view_users"><a href="../pembukuan/lihat_kegiatan.php">Kelola Daftar Kegiatan</a></li>
			<li class="icn_folder"><a href="tambah_kategori.php">Tambah Kategori</a></li>
			<li class="icn_edit_article"><a href="lihat_kategori.php">Ubah Kategori</a></li>
			<li class="icn_categories"><a href="../pembukuan/rekap_harian.php">Rekap Harian</a></li>
			<li class="icn_categories"><a href="../pembukuan/laporan_bulanan_ku.php">Laporan Keuangan Bulanan</a></li>
			
		</ul>
		<h3>Daftar Pemesanan</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="tambah_pelanggan.php">Tambah Pelanggan</a></li>
			<li class="icn_edit_article"><a href="lihat_pelanggan.php">Ubah Pelanggan</a></li>
			<li class="icn_folder"><a href="tambah_produk.php">Tambah Produk</a></li>
			<li class="icn_edit_article"><a href="lihat_produk.php">Ubah Produk</a></li>
			<li class="icn_new_article"><a href="tambah_pemesanan.php">Tambah Pemesanan</a></li>
			<li class="icn_categories"><a href="lihat_pemesanan.php">Kelola Daftar Pemesanan</a></li>
			<li class="icn_categories"><a href="laporan_bulanan_pem.php">Laporan Pemesanan Bulanan</a></li>
			<li class="icn_tags"><a href="grafik2.php">Grafik Omset</a></li>
			
		</ul>
		<h3>Admin</h3>
		<ul class="toggle">
			
			<li class="icn_jump_back"><a href="../../config/logout.php">Keluar</a></li>
		</ul>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<section id="secondary_bar">
		
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="../index.php">KARYA LOGAM</a> <div class="breadcrumb_divider"></div> <a class="current">Ubah Pelanggan</a></article>
		</div>
	</section>
		
		
		
		
		
		<div class="clear"></div>
	<div class="row">
			<div class="col-md-6">
				<?php
							$kode_pelanggan = $_GET['kode_pelanggan'];
							$query = mysql_query("SELECT * FROM pelanggan where kode_pelanggan = '$kode_pelanggan'");
							$querytrans = mysql_fetch_array($query);
							$kode_pelanggan = $querytrans['kode_pelanggan'];
							$nama_pelanggan = $querytrans['nama_pelanggan'];
							$alamat = $querytrans['alamat'];
							$catatan = $querytrans['catatan'];
							
							?>
					
					<div class="panel-body">
					<form role="form" action="update_pelanggan.php" method="GET" enctype="multipart/form-data">
								<div class="form-group">
									<input class="form-control" type="hidden" name="kode_pelanggan"  type="text" value="<?php echo $kode_pelanggan; ?>"  />
								</div>
								<div class="form-group">
									<label>Nama Pelanggan</label>
									<input class="form-control" name="pelanggan" type="text" value="<?php echo $nama_pelanggan; ?>" required oninvalid="this.setCustomValidity('kolom nama pelanggan belum terisi')" />
								</div>
								<div class="form-group">
									<label>Alamat</label>
									<textarea class="form-control" rows="7" name="alamat" required oninvalid="this.setCustomValidity('kolom alamat belum terisi')"><?php echo $alamat; ?></textarea>
								</div>
								<div class="form-group">
									<label>Catatan</label>
									<textarea class="form-control" rows="7" name="catatan" ><?php echo $catatan; ?></textarea>
								</div>
								
								
								<button type="submit" class="btn btn-primary" name="edit">Simpan</button>
								
							</form>
								
					</div>
					</div>
				
			</div><!-- /.row -->

		</div><!--/.main-->
	</section>


</body>
<script src="../js/jquery-1.11.1.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/chart.min.js"></script>
		<script src="../js/chart-data.js"></script>
		<script src="../js/easypiechart.js"></script>
		<script src="../js/easypiechart-data.js"></script>
		<script src="../js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
		<script type="text/javascript" src="../js/locales/bootstrap-datetimepicker.id.js" charset="UTF-8"></script>
		
		<script type="text/javascript">

 $('.form_date').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script>

</html>