<body onLoad="window.print()">
<style>
table {
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
}
</style>
<?php include "../../config/koneksi.php";
$bulan = $_GET['bulan'];
$tahun = $_GET['tahun'];
if($bulan == '1'){$namabulan = "Januari";}
elseif ($bulan == '2') {$namabulan = "Februari";}
elseif ($bulan == '3') {$namabulan = "Maret";}
elseif ($bulan == '4') {$namabulan = "April";}
elseif ($bulan == '5') {$namabulan = "Mei";}
elseif ($bulan == '6') {$namabulan = "Juni";}
elseif ($bulan == '7') {$namabulan = "Juli";}
elseif ($bulan == '8') {$namabulan = "Agustus";}
elseif ($bulan == '9') {$namabulan = "September";}
elseif ($bulan == '10') {$namabulan = "Oktober";}
elseif ($bulan == '11') {$namabulan = "November";}
elseif ($bulan == '12') {$namabulan = "Desember";}
?>


<table width="90%">
<div class="col-sm-6" align="center">
<label style="margin-top:20px;margin-bottom:20px;text-align:center;">Rekap Laporan Pemesanan Bulanan</label>
										<br>
<label style="margin-top:20px;margin-bottom:20px;text-align:center;"><?php echo "Bulan : ".$namabulan." | Tahun : ".$tahun; ?></label>
										<br>
</div>										<br>
<tr>
<th class="center">Tanggal</th>
										<th class="center">Nama Pelanggan</th>
										<th class="center">Nama Produk</th>
										<th class="center">Jumlah Pesanan</th>
										<th class="center">Harga</th>
										<th class="center">Total Harga</th>
										<th class="center">Keterangan</th>
</tr>

<?php
$query_tampil = mysql_query("SELECT tanggal,nama_produk,nama_pelanggan,jumlah_pesan,transaksi_pemesanan.harga, transaksi_pemesanan.harga*jumlah_pesan as total_harga,keterangan from transaksi_pemesanan, produk,pelanggan where MONTH(tanggal) = '$bulan' AND YEAR(tanggal)='$tahun' AND transaksi_pemesanan.kode_produk = produk.kode_produk AND transaksi_pemesanan.kode_pelanggan = pelanggan.kode_pelanggan ORDER BY date(tanggal), transaksi_pemesanan.kode_pelanggan");
$query_omset = mysql_query("SELECT  sum(transaksi_pemesanan.harga*jumlah_pesan) as totalOmset from transaksi_pemesanan, produk,pelanggan where MONTH(tanggal) = '$bulan' AND YEAR(tanggal)='$tahun' AND transaksi_pemesanan.kode_produk = produk.kode_produk AND transaksi_pemesanan.kode_pelanggan = pelanggan.kode_pelanggan ORDER BY date(tanggal), transaksi_pemesanan.kode_pelanggan");						
$select_omset = mysql_fetch_array($query_omset);
$totalOmset = $select_omset['totalOmset'];
while($select_tampil = mysql_fetch_array($query_tampil)){
	$tanggal1 = $select_tampil['tanggal'];
											list($tahun,$bulan,$hari) = split('-', $tanggal1);
											$tanggalfix = $hari."-".$bulan."-".$tahun;
											$harga = $select_tampil['harga'];
											$total_harga = $select_tampil['total_harga'];
											$jumlah_desimal ="0";
											$pemisah_desimal =",";
											$pemisah_ribuan =".";
											
											
											?>
											<tr class="odd gradeX">
												<td><?php echo $tanggalfix; ?></td>
												<td><?php echo $select_tampil['nama_pelanggan']; ?></td>
												<td><?php echo $select_tampil['nama_produk']; ?></td>
												<td><?php echo $select_tampil['jumlah_pesan']; ?></td>
												<td><?php echo "Rp ".number_format($harga, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></td>
												<td><?php echo "Rp ".number_format($total_harga, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></td>
												<td><?php echo $select_tampil['keterangan']; ?></td>
											</tr>
										<?php 
											} 
										
										?>			
										</table>
										<ul style="list-style-type:none">
  
  <li><b>Total Omset			: <?php echo "Rp ".number_format($totalOmset, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></b></li>
</ul>

										</body>		