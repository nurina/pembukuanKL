<!doctype html>
<html lang="en">
<?php include "../../config/koneksi.php";
ini_set("display_error","0");
error_reporting(0);
session_start();

if (empty($_SESSION['username'])) {
	header('location:../../index.php');
}
?>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISTEM INFORMASI PEMBUKUAN KARYA LOGAM</title>
	<link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen" />
	<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/datepicker3.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	

  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min1.js"></script>
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="../js/hideshow.js" type="text/javascript"></script>
	<script src="../js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../index.php"><span>SISTEM INFORMASI PEMBUKUAN</span> KARYA LOGAM</a>
			</div>

		</div><!-- /.container-fluid -->
	</nav>
	<!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<h3>Pemasukan dan Pengeluaran Dana</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="tambah_aktifitas.php">Tambah Kegiatan</a></li>
			<li class="icn_view_users"><a href="lihat_kegiatan.php">Kelola Daftar Kegiatan</a></li>
			<li class="icn_folder"><a href="tambah_kategori.php">Tambah Kategori</a></li>
			<li class="icn_edit_article"><a href="lihat_kategori.php">Ubah Kategori</a></li>
			<li class="icn_categories"><a href="rekap_harian.php">Rekap Harian</a></li>
			<li class="icn_categories"><a href="laporan_bulanan_ku.php">Laporan Keuangan Bulanan</a></li>
			
		</ul>
		<h3>Daftar Pemesanan</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="../pemesanan/tambah_pelanggan.php">Tambah Pelanggan</a></li>
			<li class="icn_edit_article"><a href="../pemesanan/lihat_pelanggan.php">Ubah Pelanggan</a></li>
			<li class="icn_folder"><a href="../pemesanan/tambah_produk.php">Tambah Produk</a></li>
			<li class="icn_edit_article"><a href="../pemesanan/lihat_produk.php">Ubah Produk</a></li>
			<li class="icn_new_article"><a href="../pemesanan/tambah_pemesanan.php">Tambah Pemesanan</a></li>
			<li class="icn_categories"><a href="../pemesanan/lihat_pemesanan.php">Kelola Daftar Pemesanan</a></li>
			<li class="icn_categories"><a href="../pemesanan/laporan_bulanan_pem.php">Laporan Pemesanan Bulanan</a></li>
			<li class="icn_tags"><a href="../pemesanan/grafik2.php">Grafik Omset</a></li>
			
		</ul>
		
		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_jump_back"><a href="../../config/logout.php">Keluar</a></li>
		</ul>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<section id="secondary_bar">
		
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="../index.php">KARYA LOGAM</a> <div class="breadcrumb_divider"></div> <a class="current">Kelola Daftar Kegiatan</a></article>
		</div>
	</section>
		<div class="clear"></div>
		<div class="panel-body">
							<form method="post" action="" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xs-6 col-sm-3">
										<label>Bulan</label>
										<select class="form-control" name="bulan">
										<option value="" >Pilih Bulan</option>
										<option value="01" >Januari</option>	
										<option value="02" >Februari</option>	
										<option value="03" >Maret</option>	
										<option value="04" >April</option>	
										<option value="05" >Mei</option>	
										<option value="06" >Juni</option>	
										<option value="07" >Juli</option>	
										<option value="08" >Agustus</option>	
										<option value="09" >september</option>	
										<option value="10" >Oktober</option>	
										<option value="11" >November</option>	
										<option value="12" >Desember</option>	
										</select>
									</div>
									<div class="col-xs-6 col-sm-3">
										<label>Tahun</label>
										<select class="form-control" name="tahun">
										<option value="" >Pilih Tahun</option>
										<?php
											$tahun=2015;
											for ($i=$tahun; $i<=$tahun +50 ; $i++)
											echo "<option value=$i>$i<br>";
											echo "</option></select>";

												?>

									</div>
									<div class="col-xs-6 col-sm-3">
										<button style="margin-top:25px" class="btn btn-primary" type="submit" name="list_tampil">Submit</button>
									</div>
									
								</div>
							</form>
							<br>

<div class="dataTable_wrapper">

								<?php 
	if(isset($_GET['isEdit'])) { ?>
		<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Kegiatan Berhasil Diubah</div>
	<?php }
	elseif(isset($_GET['isDelete'])) { ?>
	<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Kegiatan Berhasil Dihapus</div>
	<?php }
	?>


<?php
									if(isset($_POST['list_tampil'])){
										$bulan = $_POST['bulan'];
										$tahun = $_POST['tahun'];
										$query_tampil = mysql_query("SELECT kode_transaksi,tanggal,nama_aktifitas,nama_kategori,jumlah_uang from transaksi_pembukuan inner join kategori_pembukuan using(kode_kategori) where MONTH(tanggal) = '$bulan' AND YEAR(tanggal)='$tahun' ORDER BY DATE(tanggal)");
										if($bulan == '1'){$namabulan = "Januari";}
elseif ($bulan == '2') {$namabulan = "Februari";}
elseif ($bulan == '3') {$namabulan = "Maret";}
elseif ($bulan == '4') {$namabulan = "April";}
elseif ($bulan == '5') {$namabulan = "Mei";}
elseif ($bulan == '6') {$namabulan = "Juni";}
elseif ($bulan == '7') {$namabulan = "Juli";}
elseif ($bulan == '8') {$namabulan = "Agustus";}
elseif ($bulan == '9') {$namabulan = "September";}
elseif ($bulan == '10') {$namabulan = "Oktober";}
elseif ($bulan == '11') {$namabulan = "November";}
elseif ($bulan == '12') {$namabulan = "Desember";}
										?>
										<div class="col-md-4" align="left">

<label style=""><?php echo "Bulan : ".$namabulan." | Tahun : ".$tahun; ?></label>
<br>
</div>
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									
									
										
										<tr>
										<th class="center">Tanggal</th>
										<th class="center">Nama Pengeluaran / Pemasukan</th>
										<th class="center">Nama Kategori</th>
										<th class="center">Jumlah Uang</th>
										<th class="center">Aksi</th>

									</tr>
									<?php
									
										while($select_tampil = mysql_fetch_array($query_tampil)){
											$kode_transaksi = $select_tampil['kode_transaksi'];
											$tanggal1 = $select_tampil['tanggal'];
											list($tahun,$bulan,$hari) = split('-', $tanggal1);
											$tanggalfix = $hari."-".$bulan."-".$tahun;
											$jumlah_uang = $select_tampil['jumlah_uang'];
											$jumlah_desimal ="0";
											$pemisah_desimal =",";
											$pemisah_ribuan =".";
											?>
											<tr class="odd gradeX">
												<td><?php echo $tanggalfix; ?> </td>
												<td><?php echo $select_tampil['nama_aktifitas']; ?></td>
												<td><?php echo $select_tampil['nama_kategori']; ?></td>
												<td><?php echo "Rp ".number_format($jumlah_uang, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></td>
												
												<td><a href="ubah_aktifitas.php?kode_transaksi=<?php echo $kode_transaksi; ?>"><button type="button" class="btn btn-primary" style="width:80px">Ubah</button></a>
													<a href="delete_aktifitas.php?kode_transaksi=<?php echo $kode_transaksi; ?>" onclick="return confirm('Apakah anda yakin akan kegiatan ini?')"><button type="button" class="btn btn-danger"  style="width:80px">Hapus</button></a>
												</td>
											</tr>
											<?php 
												
											} 
											
										}
										elseif (isset($_GET['bulan']) && isset($_GET['tahun'])) {
											$bulan = $_GET['bulan'];
										$tahun = $_GET['tahun'];
										$query_tampil = mysql_query("SELECT kode_transaksi,tanggal,nama_aktifitas,nama_kategori,jumlah_uang from transaksi_pembukuan inner join kategori_pembukuan using(kode_kategori) where MONTH(tanggal) = '$bulan' AND YEAR(tanggal)='$tahun' ORDER BY DATE(tanggal)");
										if($bulan == '1'){$namabulan = "Januari";}
elseif ($bulan == '2') {$namabulan = "Februari";}
elseif ($bulan == '3') {$namabulan = "Maret";}
elseif ($bulan == '4') {$namabulan = "April";}
elseif ($bulan == '5') {$namabulan = "Mei";}
elseif ($bulan == '6') {$namabulan = "Juni";}
elseif ($bulan == '7') {$namabulan = "Juli";}
elseif ($bulan == '8') {$namabulan = "Agustus";}
elseif ($bulan == '9') {$namabulan = "September";}
elseif ($bulan == '10') {$namabulan = "Oktober";}
elseif ($bulan == '11') {$namabulan = "November";}
elseif ($bulan == '12') {$namabulan = "Desember";}
										?>
										<div class="col-md-4" align="left">

<label style=""><?php echo "Bulan : ".$namabulan." | Tahun : ".$tahun; ?></label>
<br>
</div>
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
										
										
									
										<tr>
										<th class="center">Tanggal</th>
										<th class="center">Nama Pengeluaran / Pemasukan</th>
										<th class="center">Nama Kategori</th>
										<th class="center">Jumlah Uang</th>
										<th class="center">Aksi</th>

									</tr>
									<?php
									while($select_tampil = mysql_fetch_array($query_tampil)){
										$kode_transaksi = $select_tampil['kode_transaksi'];
										$tanggal1 = $select_tampil['tanggal'];
											list($tahun,$bulan,$hari) = split('-', $tanggal1);
											$tanggalfix = $hari."-".$bulan."-".$tahun;
											$jumlah_uang = $select_tampil['jumlah_uang'];
											$jumlah_desimal ="0";
											$pemisah_desimal =",";
											$pemisah_ribuan =".";
											?>
											<tr class="odd gradeX">
												<td><?php echo $tanggalfix; ?> </td>
												<td><?php echo $select_tampil['nama_aktifitas']; ?></td>
												<td><?php echo $select_tampil['nama_kategori']; ?></td>
												<td><?php echo "Rp ".number_format($jumlah_uang, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></td>
												
												<td><a href="ubah_aktifitas.php?kode_transaksi=<?php echo $kode_transaksi; ?>"><button type="button" class="btn btn-primary" style="width:80px">Ubah</button></a>
													<a href="delete_aktifitas.php?kode_transaksi=<?php echo $kode_transaksi; ?>" onclick="return confirm('Apakah anda yakin akan aktifitas ini?')"><button type="button" class="btn btn-danger"  style="width:80px">Hapus</button></a>
												</td>
											</tr>
											<?php 
											} 
										}
										?>

									</table>

								</div>

		
		<div class="spacer"></div>
	</section>


</body>

</html>