<body onLoad="window.print()">
<style>
table {
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
}
</style>
<?php include "../../config/koneksi.php";
$bulan = $_GET['bulan'];
$tahun = $_GET['tahun'];
if($bulan == '1'){$namabulan = "Januari";}
elseif ($bulan == '2') {$namabulan = "Februari";}
elseif ($bulan == '3') {$namabulan = "Maret";}
elseif ($bulan == '4') {$namabulan = "April";}
elseif ($bulan == '5') {$namabulan = "Mei";}
elseif ($bulan == '6') {$namabulan = "Juni";}
elseif ($bulan == '7') {$namabulan = "Juli";}
elseif ($bulan == '8') {$namabulan = "Agustus";}
elseif ($bulan == '9') {$namabulan = "September";}
elseif ($bulan == '10') {$namabulan = "Oktober";}
elseif ($bulan == '11') {$namabulan = "November";}
elseif ($bulan == '12') {$namabulan = "Desember";}
?>


<table width="90%">
<div class="col-sm-6" align="center">
<label style="margin-top:20px;margin-bottom:20px;text-align:center;">Rekap Laporan Keuangan Bulanan</label>
										<br>
<label style="margin-top:20px;margin-bottom:20px;text-align:center;"><?php echo "Bulan : ".$namabulan." | Tahun : ".$tahun; ?></label>
										<br>
</div>										<br>
<tr>
<th>Kode Kategori</th>
<th>Jenis Pengeluaran</th>
<th >Jumlah Uang</th>
</tr>

<?php
$query_tampil = mysql_query("SELECT kode_kategori,nama_kategori,sum(jumlah_uang) as total_uang from transaksi_pembukuan inner join kategori_pembukuan using(kode_kategori) where kode_kategori != '16' and MONTH(tanggal) = '$bulan' AND YEAR(tanggal)='$tahun' Group BY kode_kategori ORDER BY kode_kategori");
$query_saldo = mysql_query("SELECT sum(jumlah_uang) as saldo from transaksi_pembukuan where kode_kategori = '1' and MONTH(tanggal) = '$bulan' and YEAR(tanggal)='$tahun'");
$ambil_saldo = mysql_fetch_array($query_saldo);
$saldo = $ambil_saldo['saldo'];
$query_pengeluaran = mysql_query("SELECT sum(jumlah_uang) as pengeluaran from transaksi_pembukuan where kode_kategori != '1' and kode_kategori != '1.1' and MONTH(tanggal) = '$bulan' and YEAR(tanggal)='$tahun'");
$ambil_pengeluaran = mysql_fetch_array($query_pengeluaran);
$pengeluaran = $ambil_pengeluaran['pengeluaran'];
$sisaSaldo = $saldo - $pengeluaran;								
while($select_tampil = mysql_fetch_array($query_tampil)){
	$total_uang = $select_tampil['total_uang'];
											$jumlah_desimal ="0";
											$pemisah_desimal =",";
											$pemisah_ribuan =".";
											
											?>
<tr class="odd gradeX">
<td><?php echo $select_tampil['kode_kategori']; ?> </td>
<td><?php echo $select_tampil['nama_kategori']; ?></td>
<td><?php echo "Rp ".number_format($total_uang, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></td>
												
												
												
											</tr>
										<?php 
											} 
										
										?>
										</table>
<h3>Ringkasan<h3>

<ul style="list-style-type:none">
  <li>Total Pemasukan	: <?php echo "Rp ".number_format($saldo, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></li>
  <li>Total Pengeluaran	: <?php echo "Rp ".number_format($pengeluaran, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></li>
  <li>Sisa				: <?php echo "Rp ".number_format($sisaSaldo, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></li>
</ul>
										</body>		