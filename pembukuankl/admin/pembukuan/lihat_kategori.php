<!doctype html>
<html lang="en">
<?php include "../../config/koneksi.php";
ini_set("display_error","0");
error_reporting(0);
session_start();

if (empty($_SESSION['username'])) {
	header('location:../../index.php');
}
?>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISTEM INFORMASI PEMBUKUAN KARYA LOGAM</title>
	<link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen" />
	<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/datepicker3.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	
<script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min1.js"></script>
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="../js/hideshow.js" type="text/javascript"></script>
	<script src="../js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../index.php"><span>SISTEM INFORMASI PEMBUKUAN</span> KARYA LOGAM</a>
			</div>

		</div><!-- /.container-fluid -->
	</nav>
	<!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<h3>Pemasukan dan Pengeluaran Dana</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="tambah_aktifitas.php">Tambah Kegiatan</a></li>
			<li class="icn_view_users"><a href="lihat_kegiatan.php">Kelola Daftar Kegiatan</a></li>
			<li class="icn_folder"><a href="tambah_kategori.php">Tambah Kategori</a></li>
			<li class="icn_edit_article"><a href="lihat_kategori.php">Ubah Kategori</a></li>
			<li class="icn_categories"><a href="rekap_harian.php">Rekap Harian</a></li>
			<li class="icn_categories"><a href="laporan_bulanan_ku.php">Laporan Keuangan Bulanan</a></li>
			
		</ul>
		<h3>Daftar Pemesanan</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="../pemesanan/tambah_pelanggan.php">Tambah Pelanggan</a></li>
			<li class="icn_edit_article"><a href="../pemesanan/lihat_pelanggan.php">Ubah Pelanggan</a></li>
			<li class="icn_folder"><a href="../pemesanan/tambah_produk.php">Tambah Produk</a></li>
			<li class="icn_edit_article"><a href="../pemesanan/lihat_produk.php">Ubah Produk</a></li>
			<li class="icn_new_article"><a href="../pemesanan/tambah_pemesanan.php">Tambah Pemesanan</a></li>
			<li class="icn_categories"><a href="../pemesanan/lihat_pemesanan.php">Kelola Daftar Pemesanan</a></li>
			<li class="icn_categories"><a href="../pemesanan/laporan_bulanan_pem.php">Laporan Pemesanan Bulanan</a></li>
			<li class="icn_tags"><a href="../pemesanan/grafik2.php">Grafik Omset</a></li>
			
		</ul>
		
		<h3>Admin</h3>
		<ul class="toggle">
			
			<li class="icn_jump_back"><a href="../../config/logout.php">Keluar</a></li>
		</ul>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<section id="secondary_bar">
		
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="../index.php">KARYA LOGAM</a> <div class="breadcrumb_divider"></div> <a class="current">Ubah Daftar Kategori</a></article>
		</div>
	</section>
		<div class="clear"></div>
		<div class="panel-body">
							<form method="post" action="" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xs-6 col-sm-3">
										<label>Cari Berdasarkan Nama Kategori</label>
									<input class="form-control" name="katakunci" type="text" required />
									</div>
									
									<div class="col-xs-6 col-sm-3">
										<button style="margin-top:25px" class="btn btn-primary" type="submit" name="cari">Submit</button>
									</div>
									
								</div>
							</form>
							<br>
<?php 
	if(isset($_GET['isSuccess'])) { ?>
		<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Kategori Berhasil Diperbarui</div>
	<?php }
	elseif(isset($_GET['isFailed'])) { ?>
	<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nama Kategori Sudah Ada</div>
	<?php }
	?>
<div class="dataTable_wrapper">
<?php
$BatasAwal = 10; 
 if (!empty($_GET['page'])) 
 	{ $hal = $_GET['page'] - 1; $MulaiAwal = $BatasAwal * $hal; } 
 else if (!empty($_GET['page']) and $_GET['page'] == 1) 
 	{ $MulaiAwal = 0; } else if (empty($_GET['page'])) { $MulaiAwal = 0; }
 	//tampil data 
 $query_tampil = mysql_query("SELECT * FROM kategori_pembukuan LIMIT $MulaiAwal , $BatasAwal"); 
 ?>
 <div class="dataTable_wrapper">
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								
								<?php
if(isset($_POST['cari'])){
$katakunci = $_POST['katakunci'];
$query_cari = mysql_query("SELECT * from kategori_pembukuan where nama_kategori like '%$katakunci%'");
?>


									
									
										<tr>
										<th class="center"><p align="center">Kode Kategori</p></th>
										<th class="center"><p align="center">Nama Kategori</p></th>
										
										<th class="center"><p align="center">Aksi</p></th>

									</tr>
									<?php
									
										while($select_tampil = mysql_fetch_array($query_cari)){
											 $kode_kategori = $select_tampil['kode_kategori'];
						
											?>
											<tr class="odd gradeX">
												<td width="20px"><?php echo $kode_kategori; ?></td>
												<td><?php echo $select_tampil['nama_kategori']; ?></td>
												
												<td align="center" width="35px"><a href="ubah_kategori.php?kode_kategori=<?php echo $kode_kategori;?>"><button type="button" class="btn btn-primary" style="width:80px">Ubah</button></a>
												</td>
											</tr>
										<?php 
											} 
											?>
</table>
											<?php
											
											

										}

else{
										?>
								

									
									
										<tr>
										<th class="center"><p align="center">Kode Kategori</p></th>
										<th class="center"><p align="center">Nama Kategori</p></th>
										
										<th class="center"><p align="center">Aksi</p></th>

									</tr>
									<?php
									
										while($select_tampil = mysql_fetch_array($query_tampil)){
											$kode_kategori = $select_tampil['kode_kategori'];
											?>
											<tr class="odd gradeX">
												<td width="20px"><?php echo $kode_kategori; ?></td>
												<td><?php echo $select_tampil['nama_kategori']; ?></td>
												<td align="center" width="35px"><a href="ubah_kategori.php?kode_kategori=<?php echo $kode_kategori;?>"><button type="button" class="btn btn-primary" style="width:80px">Ubah</button></a>
												</td>
											</tr>
										<?php 
											} 
										
										?>			
									</table>

								<?php
									
									//navigasi 
 	$cekQuery = mysql_query("SELECT * FROM kategori_pembukuan"); 
 	$jumlahData = mysql_num_rows($cekQuery); 
 	if ($jumlahData > $BatasAwal) 
 		{ echo '<br/><center>'; 
 	$a = explode(".", $jumlahData / $BatasAwal); 
 	$b = $a[0];
 	 $c = $b + 1;
 	echo  '<ul class="pagination">';
 	  for ($i = 1; $i <= $c; $i++) 
 	  	{ echo '<li><a '; 
 	  if ($_GET['page'] == $i) { echo 'color:red'; } 
 	  

 	  echo '" href="?page=' . $i . '">' . $i . '</a></li> '; } echo '</ul></center>'; } 


										}
										
										?>		
		</div>


		
		<div class="spacer"></div>
	</section>


</body>

</html>