<!doctype html>
<html lang="en">
<?php include "../../config/koneksi.php";
ini_set("display_error","0");
error_reporting(0);
session_start();

if (empty($_SESSION['username'])) {
	header('location:../../index.php');
}
?>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISTEM INFORMASI PEMBUKUAN KARYA LOGAM</title>
	<link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen" />
	<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/datepicker3.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	

	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="../js/hideshow.js" type="text/javascript"></script>
	<script src="../js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../index.php"><span>SISTEM INFORMASI PEMBUKUAN</span> KARYA LOGAM</a>
			</div>

		</div><!-- /.container-fluid -->
	</nav>
	<!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<h3>Pemasukan dan Pengeluaran Dana</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="tambah_aktifitas.php">Tambah Kegiatan</a></li>
			<li class="icn_view_users"><a href="lihat_kegiatan.php">Kelola Daftar Kegiatan</a></li>
			<li class="icn_folder"><a href="tambah_kategori.php">Tambah Kategori</a></li>
			<li class="icn_edit_article"><a href="lihat_kategori.php">Ubah Kategori</a></li>
			<li class="icn_categories"><a href="rekap_harian.php">Rekap Harian</a></li>
			<li class="icn_categories"><a href="laporan_bulanan_ku.php">Laporan Keuangan Bulanan</a></li>
			
		</ul>
		<h3>Daftar Pemesanan</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="../pemesanan/tambah_pelanggan.php">Tambah Pelanggan</a></li>
			<li class="icn_edit_article"><a href="../pemesanan/lihat_pelanggan.php">Ubah Pelanggan</a></li>
			<li class="icn_folder"><a href="../pemesanan/tambah_produk.php">Tambah Produk</a></li>
			<li class="icn_edit_article"><a href="../pemesanan/lihat_produk.php">Ubah Produk</a></li>
			<li class="icn_new_article"><a href="../pemesanan/tambah_pemesanan.php">Tambah Pemesanan</a></li>
			<li class="icn_categories"><a href="../pemesanan/lihat_pemesanan.php">Kelola Daftar Pemesanan</a></li>
			<li class="icn_categories"><a href="../pemesanan/laporan_bulanan_pem.php">Laporan Pemesanan Bulanan</a></li>
			<li class="icn_tags"><a href="../pemesanan/grafik2.php">Grafik Omset</a></li>
			
		</ul>
		
		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_jump_back"><a href="../../config/logout.php">Keluar</a></li>
		</ul>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<section id="secondary_bar">
		
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="../index.php">KARYA LOGAM</a> <div class="breadcrumb_divider"></div> <a class="current">Rekap Harian</a></article>
		</div>
	</section>
		<div class="clear"></div>
		<div class="panel-body">
							<form method="post" action="" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xs-6 col-sm-3">
										<div class="form-group">
                <label>Tanggal</label>
                <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input class="form-control" size="10" type="date" name="tanggal">
     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
    <input type="hidden" id="dtp_input2" value=""/>
 
            </div>
									</div>
									
									<div class="col-xs-6 col-sm-3">
										<button style="margin-top:25px" class="btn btn-primary" type="submit" name="list_tampil">Submit</button>
									</div>
									
								</div>
							</form>
							
<div class="dataTable_wrapper">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">

									<?php
									if(isset($_POST['list_tampil'])){
										$tanggal = $_POST['tanggal'];
										
										$query_tampil = mysql_query("SELECT tanggal,nama_aktifitas,nama_kategori,jumlah_uang from transaksi_pembukuan inner join kategori_pembukuan using(kode_kategori) where tanggal = '$tanggal' ORDER BY kode_kategori");
										$query_saldo = mysql_query("SELECT sum(jumlah_uang) as saldo from transaksi_pembukuan where kode_kategori = '1' and tanggal = '$tanggal'");
										$query_ss = mysql_query("SELECT sum(jumlah_uang) as ss from transaksi_pembukuan where kode_kategori = '1.1' and tanggal = '$tanggal'");
$ambil_saldo = mysql_fetch_array($query_saldo);
$ambil_ss = mysql_fetch_array($query_ss);

$saldo = $ambil_saldo['saldo'] + $ambil_ss['ss'];
										$query_pengeluaran = mysql_query("SELECT sum(jumlah_uang) as pengeluaran from transaksi_pembukuan where kode_kategori != '1' and kode_kategori != '1.1' and tanggal = '$tanggal'");
										$ambil_pengeluaran = mysql_fetch_array($query_pengeluaran);
										$pengeluaran = $ambil_pengeluaran['pengeluaran'];
										$sisaSaldo = $saldo - $pengeluaran;
										?>
										
									
										<tr>
										<th class="center"><p align="center">Tanggal</p></th>
										<th class="center"><p align="center">Nama Pengeluaran / Pemasukan</p></th>
										<th class="center"><p align="center">Nama Kategori</p></th>
										<th class="center"><p align="center">Jumlah Uang</p></th>
										
									</tr>
									<?php
										while($select_tampil = mysql_fetch_array($query_tampil)){
											$tanggal1 = $select_tampil['tanggal'];
											list($tahun,$bulan,$hari) = split('-', $tanggal1);
											$tanggalfix = $hari."-".$bulan."-".$tahun;
											$jumlah_uang = $select_tampil['jumlah_uang'];
											$jumlah_desimal ="0";
											$pemisah_desimal =",";
											$pemisah_ribuan =".";
											?>
											<tr class="odd gradeX">
												<td><?php echo $tanggalfix; ?> </td>
												<td><?php echo $select_tampil['nama_aktifitas']; ?></td>
												<td><?php echo $select_tampil['nama_kategori']; ?></td>
												<td><?php echo "Rp ".number_format($jumlah_uang, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></td>
												
											</tr>
										<?php 
											} 
?>
									</table>

								</div>
<div class="row">
<div class="col-md-4">
<div class="panel panel-default">
	<div class="panel-heading">Ringkasan</div>
  <div class="panel-body">
  <ul class="list-group">
  <li class="list-group-item">
    <span class="badge"><?php echo $tanggalfix; ?></span>
    Tanggal
  </li>
  <li class="list-group-item">
    <span class="badge"><?php echo "Rp ".number_format($saldo, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></span>
    Total Pemasukan
  </li>
  <li class="list-group-item">
    <span class="badge"><?php echo "Rp ".number_format($pengeluaran, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?></span>
    Total Pengeluaran
  </li>
  <li class="list-group-item">
    <span class="badge"><?php echo "Rp ".number_format($sisaSaldo, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan); ?> </span>
   Sisa Saldo
  </li>
</ul>
  </div>
</div>
</div>
</div>

		
		<div class="spacer"></div>
	</section>
<?php
}
										
										?>	
</body>
<script src="../js/jquery-1.11.1.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/chart.min.js"></script>
		<script src="../js/chart-data.js"></script>
		<script src="../js/easypiechart.js"></script>
		<script src="../js/easypiechart-data.js"></script>
		<script src="../js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
		<script type="text/javascript" src="../js/locales/bootstrap-datetimepicker.id.js" charset="UTF-8"></script>
		
		<script type="text/javascript">

 $('.form_date').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script>

</html>