<!doctype html>
<html lang="en">
<?php include "../../config/koneksi.php";
ini_set("display_error","0");
error_reporting(0);
session_start();

if (empty($_SESSION['username'])) {
	header('location:../../index.php');
}?>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISTEM INFORMASI PEMBUKUAN KARYA LOGAM</title>
	<link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen" />
	<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/datepicker3.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	

	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="../js/hideshow.js" type="text/javascript"></script>
	<script src="../js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

<script language="javascript">
var s5_taf_parent = window.location;
    function open_win_editar(targetBulan,targetTahun) {
        window.open ('preview_pem.php?bulan=' + targetBulan + '&tahun=' + targetTahun, 'page','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=750,height=600,left=50,top=50,titlebar=yes');
     }
</script>

</head>


<body>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../index.php"><span>SISTEM INFORMASI PEMBUKUAN</span> KARYA LOGAM</a>
			</div>

		</div><!-- /.container-fluid -->
	</nav>
	<!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<h3>Pemasukan dan Pengeluaran Dana</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="../pembukuan/tambah_aktifitas.php">Tambah Kegiatan</a></li>
			<li class="icn_view_users"><a href="../pembukuan/lihat_kegiatan.php">Kelola Daftar Kegiatan</a></li>
			<li class="icn_folder"><a href="tambah_kategori.php">Tambah Kategori</a></li>
			<li class="icn_edit_article"><a href="lihat_kategori.php">Ubah Kategori</a></li>
			<li class="icn_categories"><a href="../pembukuan/rekap_harian.php">Rekap Harian</a></li>
			<li class="icn_categories"><a href="../pembukuan/laporan_bulanan_ku.php">Laporan Keuangan Bulanan</a></li>
			
		</ul>
		<h3>Daftar Pemesanan</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="tambah_pelanggan.php">Tambah Pelanggan</a></li>
			<li class="icn_edit_article"><a href="lihat_pelanggan.php">Ubah Pelanggan</a></li>
			<li class="icn_folder"><a href="tambah_produk.php">Tambah Produk</a></li>
			<li class="icn_edit_article"><a href="lihat_produk.php">Ubah Produk</a></li>
			<li class="icn_new_article"><a href="tambah_pemesanan.php">Tambah Pemesanan</a></li>
			<li class="icn_categories"><a href="lihat_pemesanan.php">Kelola Daftar Pemesanan</a></li>
			<li class="icn_categories"><a href="laporan_bulanan_pem.php">Laporan Pemesanan Bulanan</a></li>
			
		</ul>

		<h3>Grafik</h3>
		<ul class="toggle">
			<li class="icn_categories"><a href="grafik_omset.php">Grafik Omset</a></li>
			<li class="icn_categories"><a href="laporan_bulanan_pem.php">Laporan Pemesanan Bulanan</a></li>
			
		</ul>
		
		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_jump_back"><a href="../../config/logout.php">Keluar</a></li>
		</ul>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<section id="secondary_bar">
		
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="../index.php">KARYA LOGAM</a> <div class="breadcrumb_divider"></div> <a class="current">Laporan Pemesanan Bulanan</a></article>
		</div>
	</section>
		
		
		
		
		
		<div class="clear"></div>
		
		<div class="panel-body">
							<form method="post" action="" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xs-6 col-sm-3">
										<label>Semester</label>
										<select class="form-control" name="semester">
										<option value="" >Pilih Semester</option>
										<option value="1" >Semester 1</option>	
										<option value="2" >Semester 2</option>	
										</select>
									</div>
									<div class="col-xs-6 col-sm-3">
										<label>Tahun</label>
										<select class="form-control" name="tahun">
										<option value="" >Pilih Tahun</option>
										<?php
											$tahun=2015;
											for ($i=$tahun; $i<=$tahun +50 ; $i++)
											echo "<option value=$i>$i<br>";
											echo "</option></select>";

												?>

									</div>
									<div class="col-xs-6 col-sm-3">
										<button style="margin-top:25px" class="btn btn-primary" type="submit" name="list_tampil">Submit</button>
									</div>
									
								</div>
							</form>
							<br>

<div class="dataTable_wrapper">
<?php
									if(isset($_POST['list_tampil'])){
										$semester = $_POST['semester'];
										$tahun = $_POST['tahun'];
										?>
										<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/highcharts.js" type="text/javascript"></script>
<script type="text/javascript">
	var chart1; // globally available
$(document).ready(function() {
      chart1 = new Highcharts.Chart({
         chart: {
            renderTo: 'container',
            type: 'column'
         },   
         title: {
            text: 'Grafik Omset Penjualan '
         },
         xAxis: {
            categories: ['Bulan']
         },
         yAxis: {
            title: {
               text: 'Jumlah terjual'
            }
         },
              series:             
            [
            <?php 

        	
           $sql   = "SELECT month(tanggal)  FROM transaksi_pemesanan where month(tanggal) >= 1 AND month(tanggal) <= 6 ";
            $query = mysql_query($sql) ;
            while( $ret = mysql_fetch_array( $query ) ){
            	$merek=$ret['merek'];                     
                 $sql_jumlah   = "SELECT  sum(transaksi_pemesanan.harga*jumlah_pesan) as totalOmset from transaksi_pemesanan, produk,pelanggan where MONTH(tanggal) >= 1 AND month(tanggal) <= 6 AND YEAR(tanggal)='$tahun' AND transaksi_pemesanan.kode_produk = produk.kode_produk AND transaksi_pemesanan.kode_pelanggan = pelanggan.kode_pelanggan";        
                 $query_jumlah = mysql_query( $sql_jumlah );
                 while( $data = mysql_fetch_array( $query_jumlah ) ){
                    $jumlah = $data['jumlah'];                 
                  }             
                  ?>
                  {
                      name: '<?php echo $merek; ?>',
                      data: [<?php echo $jumlah; ?>]
                  },
                  <?php } ?>
            ]
      });
   });	

</script>

																		</div>
		
		<div class="spacer"></div>
	</section>


</body>

</html>