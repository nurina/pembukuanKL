<!doctype html>
<html lang="en">
<?php
ini_set("display_error","0");
error_reporting(0);
session_start();

if (empty($_SESSION['username'])) {
	header('location:../index.php');
}
?>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>SISTEM INFORMASI PEMBUKUAN KARYA LOGAM</title>
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/datepicker3.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	

	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php"><span>SISTEM INFORMASI PEMBUKUAN</span> KARYA LOGAM</a>
			</div>

		</div><!-- /.container-fluid -->
	</nav>
	<!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		
		<h3>Pemasukan dan Pengeluaran Dana</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="pembukuan/tambah_aktifitas.php">Tambah Kegiatan</a></li>
			<li class="icn_view_users"><a href="pembukuan/lihat_kegiatan.php">Kelola Daftar Kegiatan</a></li>
			<li class="icn_folder"><a href="tambah_kategori.php">Tambah Kategori</a></li>
			<li class="icn_edit_article"><a href="lihat_kategori.php">Ubah Kategori</a></li>
			<li class="icn_categories"><a href="pembukuan/rekap_harian.php">Rekap Harian</a></li>
			<li class="icn_categories"><a href="pembukuan/laporan_bulanan_ku.php">Laporan Keuangan Bulanan</a></li>
			
		</ul>
		<h3>Daftar Pemesanan</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="pemesanan/tambah_pelanggan.php">Tambah Pelanggan</a></li>
			<li class="icn_edit_article"><a href="pemesanan/lihat_pelanggan.php">Ubah Pelanggan</a></li>
			<li class="icn_folder"><a href="pemesanan/tambah_produk.php">Tambah Produk</a></li>
			<li class="icn_edit_article"><a href="pemesanan/lihat_produk.php">Ubah Produk</a></li>
			<li class="icn_new_article"><a href="pemesanan/tambah_pemesanan.php">Tambah Pemesanan</a></li>
			<li class="icn_categories"><a href="pemesanan/lihat_pemesanan.php">Kelola Daftar Pemesanan</a></li>
			<li class="icn_categories"><a href="pemesanan/laporan_bulanan_pem.php">Laporan Pemesanan Bulanan</a></li>
			<li class="icn_tags"><a href="pemesanan/grafik2.php">Grafik Omset</a></li>
			
		</ul>
		
		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_jump_back"><a href="../config/logout.php">Keluar</a></li>
		</ul>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
		<section id="secondary_bar">
		
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">KARYA LOGAM</a> <div class="breadcrumb_divider"></div> <a class="current">Halaman Utama</a></article>
		</div>
	</section>
		
		
		
		
		
		<div class="clear"></div>
		
		
		
		
		
		<article class="module width_full">
			<header><h3>SELAMAT DATANG</h3></header>
				<div class="module_content">
					
					<p>Selamat datang di sistem informasi pembukuan perusahaan Karya Logam</p>

<p>Di dalam sitem ini anda dapat melakukan kegiatan sebagai berikut :</p>

					<ul>
						<li>Menambah data pemasukan dan pengeluaran</li>
						<li>Melihat laporan pemasukan dan pengeluaran uang tiap bulan</li>
						<li>Menambah data pemesanan barang</li>
						<li>Menambah data pelanggan yang memesan barang</li>
						<li>Menambah data produk yang dijual</li>
					</ul>
				</div>
		</article><!-- end of styles article -->
		<div class="spacer"></div>
	</section>


</body>

</html>