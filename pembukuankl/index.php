<!DOCTYPE html>

<html>
<?php
ini_set("display_error","0");
error_reporting(0);

include "koneksi.php";

session_start();

if ( !empty($_SESSION['username'])) {
    header('location:admin/index.php');
}

?>
    <head>
        <meta charset="UTF-8">
        <title>Log in | SISTEM INFORMASI PEMBUKUAN KL</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="admin/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="admin/css/blue.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-page" >
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b>KARYA</b>LOGAM</a>
            </div><!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">LOGIN</p>
                <form action="config/cek_login.php" method="POST">
                    <div class="form-group has-feedback">
                    <label>Username</label>
                        <input type="text" class="form-control" name="user" placeholder="username" required/>
                        
                    </div>
                    <div class="form-group has-feedback">
                    <label>Password</label>
                        <input type="password" class="form-control" name="pass" placeholder="Password" required/>
                        
                    </div>
                      <div class="social-auth-links text-center">

                        <a href="register.php">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
                            </a>
                            </div>
              </form>

             
              
        </div><!-- /.login-box -->
		<hr style="margin-top:20%">
	<footer>
		<div class="row" >
			<div align="center">
				<p><b>Copyright &copy Template By medialoot & AdminLTE.</b></p>
			</div>
		</div>
	</footer>

        <!-- jQuery 2.1.3 -->
        <script src="admin/js/jQuery-2.1.3.min.js"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="admin/js/icheck.min.js" type="text/javascript"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>
